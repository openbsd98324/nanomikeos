#!/bin/sh

# This script assembles the MikeOS bootloader, kernel and programs
# with NASM, and then creates floppy and CD images (on Linux)

# Only the root user can mount the floppy disk image as a virtual
# drive (loopback mounting), in order to copy across the files

# (If you need to blank the floppy image: 'mkdosfs disk_images/mikeos.flp')



echo ">>> Assembling kernel..."

# cd source
# nasm -O0 -w+orphan-labels -f bin -o ../kernel.bin kernel.asm || exit
 nasm -O0 -w+orphan-labels -f bin -o ../kernel.bin bootloader/bootloader.asm 

 ls -ltra kernel.bin

cd ..


exit
echo ">>> Assembling programs..."

cd programs

for i in *.asm
do
	nasm -O0 -w+orphan-labels -f bin $i -o `basename $i .asm`.bin || exit
done


ls bootloader/bootload.asm
